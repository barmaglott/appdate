package ru.barmaglott.services;

import org.springframework.mobile.device.Device;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.text.ParseException;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by barmaglott on 02.02.2017.
 */
public class ServiceTest extends Assert {
    private DiffDateService diffDateService;
    private DeviceService deviceService;
    @BeforeMethod
    public void upInit() {
        diffDateService = new DiffDateService();
        deviceService = new DeviceService();
    }

    @Test
    public void testLeftTimeHappyNY() {
//        String format = "dd.MM.yyyy";
//        SimpleDateFormat formaterr = new SimpleDateFormat(format);
//        Date dateMock = mock(Date.class);
//        when(dateMock.getTime()).thenReturn(anyLong());
        long actual = diffDateService.leftTimeHappyNY();
//        verify(dateMock).getTime();
        assertNotNull(actual);
    }
    @DataProvider
    public Object [][] differentBetweenDates(){
        return new Object[][]{
                {"21.01.2017",1L},
                {"20.01.2017",2L},
                {"27.01.2017",5L},
//                {"2222222222",3L}
        };
    }

    @Test(dataProvider = "differentBetweenDates")
    public void testDifferentBetweenDates(String start,long expected) {
//        String date1 = mock(String.class);
//        String date2 = mock(String.class);

        long actual = diffDateService.differentBetweenDates(start, "22.01.2017");
        assertEquals(actual, expected);
    }
    @DataProvider
    public Object[][] getDeviceData(){
        return new Object[][]{
            {"windows", "Чё мобилу в школе отжали? </br>Виндоусятников-то развелось! Куда только Майкрософт смотрит? </br>"},
            {"mac", "Чё мобилу в школе отжали? "+"</br>"+"О да у нас тут мажор?" +"</br>"},
            {"x11","Чё мобилу в школе отжали? "+"</br>"+"Чё-то пингвинятиной завоняло... ой да это ты зашел! " +"</br>"},
                {"android","Чё мобилу в школе отжали? "+"</br>"+" Наконец-то чёткий пацанчик зашёл. Слушай тут недавно мажор заглядывал хочешь я тебе его IP скину? " +"</br>"},
                {"iphone","Чё мобилу в школе отжали? "+"</br>"+"Да еще и с айфона?! Установи нормальную ось! " +"</br>"},
                {"test", "Чё мобилу в школе отжали? "+"</br>"+"UnKnown, More-Info: test</br>"}
        };
    }
    @Test(dataProvider = "getDeviceData")
    public void tesGetDevice(String userAgent,String expected){
//    @Test
//    public void tesGetDevice(){
        Device deviceMock = mock(Device.class);
//        String userAgentMock = "windows";
        /*switch (device) {
            case 1:
                when(deviceMock.isNormal()).thenReturn(true);
                break;
            case 2:
                when(deviceMock.isMobile()).thenReturn(true);
                break;
            case 3:
                when(deviceMock.isTablet()).thenReturn(true);
                break;
        }*/
        when(deviceMock.isTablet()).thenReturn(true);
        String actual = deviceService.getDevice(deviceMock, userAgent);
//        String response = "Чё мобилу в школе отжали? "+"</br>"+"О да у нас тут мажор?"+"</br>";
        verify(deviceMock).isNormal();
        verify(deviceMock).isMobile();
        verify(deviceMock).isTablet();
//        assertNotNull(actual);
        assertEquals(actual,expected);
    }
}
