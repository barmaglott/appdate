package ru.barmaglott.services;

import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.barmaglott.error.ErrorMessage;

/**
 * Created by barmaglott on 08.02.2017.
 */
public class CreateMessageTest extends Assert {
    private CreateMessage createMessage;

    @BeforeMethod
    public void Init() {
        createMessage = new CreateMessage();
    }

    @DataProvider
    public Object[][] getNumberData() {
        return new Object[][]{
                {"1111", "111"},
                {"1111", "11111"},
                {"22", "2"}
        };
    }

    @Test(dataProvider = "getNumberDate", expectedExceptions = ErrorMessage.class)
    public void testGetNumber(String number1, String number2) {
        boolean actual = false;
        try {
            actual = createMessage.getNumber(number1, number2);
        } catch (ErrorMessage exception) {
            System.out.println(exception.getMessage());
        }
        assertTrue(actual);
    }

}
