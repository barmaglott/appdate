package ru.barmaglott.error;

/**
 * Created by barmaglott on 08.02.2017.
 */
public class ErrorMessage extends Exception {
    private String message;
    public ErrorMessage(String exception){
        message = exception;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
