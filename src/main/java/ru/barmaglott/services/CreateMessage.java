package ru.barmaglott.services;

import org.springframework.stereotype.Service;
import ru.barmaglott.error.ErrorMessage;

/**
 * Created by barmaglott on 08.02.2017.
 */
@Service
public class CreateMessage {
    public boolean getNumber(String number1,String number2) throws ErrorMessage {
        if (Integer.parseInt(number1)>Integer.parseInt(number2)){
            throw new ErrorMessage("Первое число больше");
        }
        if(Integer.parseInt(number1)<Integer.parseInt(number2)){
            throw new ErrorMessage("Второе число больше");
        }
        return true;
    }
}
