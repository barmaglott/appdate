package ru.barmaglott.services;

import org.springframework.mobile.device.Device;
import org.springframework.stereotype.Service;

/**
 * Created by barmaglott on 18.12.2016.
 */
@Service
public class DeviceService {
    public String getDevice(Device device, String userAgent) {
        String response = "";
        String deviceType = "unknown";
        if (device.isNormal()) {
            deviceType = "normal";
            response = "Чё в 2016 на desktop-е сёрфишь? ";
        } else if (device.isMobile()) {
            deviceType = "mobile";
            response = "Чё на планшет денег не хватает? ";
        } else if (device.isTablet()) {
            deviceType = "tablet";
            response = "Чё мобилу в школе отжали? ";
        }
        String user = userAgent.toLowerCase();
        String os = "";
        String responseOS = "";
//        String browser = "";

//        log.info("User Agent for the request is===>"+browserDetails);
        //=================OS=======================
        if (userAgent.toLowerCase().indexOf("windows") >= 0) {
            os = "Windows";
            responseOS = "Виндоусятников-то развелось! Куда только Майкрософт смотрит? ";
        } else if (userAgent.toLowerCase().indexOf("mac") >= 0) {
            os = "Mac";
            responseOS = "О да у нас тут мажор?";
        } else if (userAgent.toLowerCase().indexOf("x11") >= 0) {
            os = "Unix";
            responseOS = "Чё-то пингвинятиной завоняло... ой да это ты зашел! ";
        } else if (userAgent.toLowerCase().indexOf("android") >= 0) {
            os = "Android";
            responseOS = " Наконец-то чёткий пацанчик зашёл. Слушай тут недавно мажор заглядывал хочешь я тебе его IP скину? ";
        } else if (userAgent.toLowerCase().indexOf("iphone") >= 0) {
            os = "IPhone";
            responseOS = "Да еще и с айфона?! Установи нормальную ось! ";
        } else {
            os = "Unknow";
            responseOS = "UnKnown, More-Info: " + userAgent;
        }
        return response + "</br>" + responseOS + "</br>";

    }
}
