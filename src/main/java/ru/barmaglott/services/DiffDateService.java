package ru.barmaglott.services;

import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by barmaglott on 18.12.2016.
 */
@Service
public class DiffDateService {
    private final static String format = "dd.MM.yyyy";
    private static SimpleDateFormat formaterr = new SimpleDateFormat(format);
    private static Date dateNow = new Date();

    public long leftTimeHappyNY() {
        long diff = 0;
        try {
            Date dateHNY = formaterr.parse("01.01.2017");
            diff = dateHNY.getTime()-dateNow.getTime();
        }catch (Exception exception){
            exception.printStackTrace();
        }
        return diff/(1000*60*60*24);
    }

    public long differentBetweenDates(String dateString1, String dateString2) {

        Date date1 = null, date2 = null;
        try {
            date1 = formaterr.parse(dateString1);
            date2 = formaterr.parse(dateString2);
        } catch (Exception exception) {
            System.out.println("Неверный формат данных.");
            exception.printStackTrace();
        }
        long different = date2.before(date1) ? (date1.getTime() - date2.getTime()) : (date2.getTime() - date1.getTime());
        return different / (1000 * 60 * 60 * 24);
    }
}
