package ru.barmaglott.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mobile.device.Device;
import org.springframework.web.bind.annotation.*;
import ru.barmaglott.error.ErrorMessage;
import ru.barmaglott.services.CreateMessage;
import ru.barmaglott.services.DeviceService;
import ru.barmaglott.services.DiffDateService;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
//import org.springframework.mobile.device.Device;

/**
 * Created by barmaglott on 18.12.2016.
 */
@RestController
public class DateController {
    @Autowired
    DiffDateService diffDateService;
    @Autowired
    DeviceService deviceService;
    @Autowired
    CreateMessage createMessage;

    @RequestMapping("/")
    public
    @ResponseBody
    String getDiffDate(Device device, @RequestHeader(value = "User-Agent") String userAgent) {


        String rezult = "";

        rezult += deviceService.getDevice(device, userAgent);
        rezult += "До нового года осталось : " + String.valueOf(diffDateService.leftTimeHappyNY()) + "дней." + "</br>\n";
        rezult += "разница между датами" + String.valueOf(diffDateService.differentBetweenDates("24.10.2017", "12.03.2018"))+"дней";
        return rezult;

    }
    @RequestMapping(value = "/diff/{number1},{number2}", method = RequestMethod.GET)
    @ResponseBody
    public String getDiffNumber(@PathVariable String number1, @PathVariable String number2){
        String rezult = "";

        try{
            createMessage.getNumber(number1,number2);
            rezult="равны";
        }catch (ErrorMessage message){
            rezult = message.getMessage();
        }
        return rezult;
    }

    @RequestMapping(value = "/swagger/example")
    @GET
    @Consumes("application/json")
    public Response swaggerTest() {
        return Response.ok().entity("SUCCESS").build();
    }
}
